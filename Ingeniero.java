import java.util.ArrayList;
import java.util.List;

/*****************
 * Autor: Emil Rueda
 * Empresa: Sociedad Antioqueña de aviación
 * Ciudad: Bello
 * Descripción: Construye aviones
 */

public class Ingeniero {
    /***********
     * Atributos
     ***********/

     private String nombre;
     private String apellido;
     private String cedula;
     //el ingeniero tiene como atributo el arreglo de aviones que fabricó
     //private Avion_militar[] aviones;

     private List<Avion_militar> aviones;

    /***********
     * Constructor
       
    ***********/
   public Ingeniero(){
       //inicializo el arreglo para que pueda guardar 10 aviones
       //this.aviones = new Avion_militar[10];

       this.aviones = new ArrayList<Avion_militar>();
     


   }
     /***********
     *Consultor
     ***********/

     public Avion_militar getAvionMilitar(int i){
         return this.aviones.get(i);
     }

    /***********
     * Métodos
     ***********/

     public boolean construir_avion(String color, double tamaño, int misiles){
         //Aquí se realiza una asociación (El ingeniero puede consultar la clase avión militar
         //pero esta no lo puede hacer). 
        Avion_militar objAvion_militar = new Avion_militar(color, tamaño, misiles);
        //Guardo los aviones creados en el arreglo
        this.aviones.add(objAvion_militar);

        return true;
     }

    
}
