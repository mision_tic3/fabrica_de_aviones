/*****************
 * Autor: Emil Rueda
 * Empresa: Sociedad Antioqueña de aviación
 * Ciudad: Bello
 * Descripción: 
 */
public class Avion {

   

      /***********
     * Atributos
     ***********/
    private String color;
    private double tamaño;

    /***********
   * Constructores
   ***********/

   public Avion(String color, double tamaño){
       this.color = color;
       this.tamaño = tamaño;
   }

   public Avion(){

   }


    /********
   * Métodos
   **********/
  public void aterrizar(){
      System.out.println("Aterrizando...");

  }

  public boolean despegar(){
    System.out.println("Despegando...");
    return true;
  }

  public void frenar(){
    System.out.println("Frenando...");

  }

    
}
