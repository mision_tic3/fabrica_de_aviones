/*****************
 * Autor: Emil Rueda
 * Empresa: Sociedad Antioqueña de aviación
 * Ciudad: Bello
 * Descripción: Aviones militares a operar
 */
public class Avion_militar extends Avion {
    /************
     * Atributos
     ***********/
    private int misiles;


    /*************
     * Constructor
     *************/
     public Avion_militar(String color, double tamaño, int misiles){
        super(color, tamaño);

    }

     public Avion_militar(String color, int tamaño){
         super(color, tamaño);

     }
    /*************
     * Setters
     *************/

     public void setMisiles(int misiles){
         this.misiles = misiles;
     }
     
     /*************
     * Getters
     *************/

     public int getMisiles(){
        return this.misiles;
    }

    /*************
     * Métodos
     *************/



     private void disparar(){
         if(this.misiles > 0){
            System.out.println("Disparando...");
            --this.misiles;
         }
         else{
            System.out.println("No hay misiles!");
         }
        }

    public void amenaza(boolean amenaza){
        if(amenaza){
            this.disparar();
        }else{
            System.out.println("No es una amenaza");
        }
    }




}
