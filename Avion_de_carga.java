/*****************
 * Autor: Emil Rueda
 * Empresa: Sociedad Antioqueña de aviación
 * Ciudad: Bello
 * Descripción: Aviones de carga a operar
 */
public class Avion_de_carga extends Avion{

    /***********
     * Atributos
     ***********/
    

    /***********
   * Constructor
   ***********/
  public Avion_de_carga(String color, double tamaño){
      super(color, tamaño);

  }

    /********
    * Métodos
    **********/

    public void cargar(){
        System.out.println("Cargando...");
    }

    public void descargar(){
        System.out.println("Descargando");
    }
    
} 
