public class App {
    public static void main(String[] args) throws Exception {
        //System.out.println("Hello, World!");
        //Avion objAvion = new Avion();
        /*
        Avion_de_carga objAvion_de_carga = new Avion_de_carga("Rojo", 150.5);
        objAvion_de_carga.cargar();
        System.out.println(objAvion_de_carga.despegar()); 
        objAvion_de_carga.aterrizar();
        objAvion_de_carga.descargar();
        Avion_de_pasajeros objAvion_de_pasajeros = new Avion_de_pasajeros("Verde", 125.8);
        */
        Avion_de_pasajeros objAvion_de_pasajeros = new Avion_de_pasajeros("Rojo", 150.5);
        objAvion_de_pasajeros.setPasajeros(10000);
        //System.out.println(objAvion_de_pasajeros.pasajeros);
        System.out.println(objAvion_de_pasajeros.getPasajeros());
        Avion_militar objAvion_militar = new Avion_militar("Verde", 120);
        objAvion_militar.setMisiles(8);
        System.out.println(objAvion_militar.getMisiles());

        objAvion_militar.amenaza(true);
        System.out.println(objAvion_militar.getMisiles());
        
        for(int i = 0; i<10; i++){
            objAvion_militar.amenaza(true);
        }

        Ingeniero objIngeniero = new Ingeniero();
        objIngeniero.construir_avion("Verde", 10, 8);
        System.out.println("----------Avión construído-------------");
        System.out.println(objIngeniero.getAvionMilitar(0));
        System.out.println("--------Accediendo al avión------------");
        System.out.println(objIngeniero.getAvionMilitar(0).despegar());




        
    }
}
