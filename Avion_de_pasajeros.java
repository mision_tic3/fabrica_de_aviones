/*****************
 * Autor: Emil Rueda
 * Empresa: Sociedad Antioqueña de aviación
 * Ciudad: Bello
 * Descripción: Aviones de pasajeros a operar
 */
public class Avion_de_pasajeros extends Avion{

    /*************
     * Atributos
     ************/
    private int pasajeros;
    



     /*************
     *Constructor
     ************/
    public Avion_de_pasajeros(String color, double tamaño){
        super(color, tamaño);
    }

    /************
     * Setter
     ***********/

     public void setPasajeros(int pasajeros){
         this.pasajeros = pasajeros;
     }
    
    /************
     * Getter
     ***********/

    public int getPasajeros(){
        return this.pasajeros;
    }


     
     /*************
     *Métodos
     ************/  
    public void servir(){
        System.out.println("Sirviendo...");
    }



    
}
